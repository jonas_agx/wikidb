# wikidb

key/value storage for wiki content

## To run this project you will need
* Golang
* HTTPpie or CURl
* Docker
* docker-compose
* make

Assuming you have all the listed dependencies above, run:
```shell
make all
```

It will build and run the application.

## Storing a JSON blob
To store a JSON document for the key `test` run the following command:

```shell
echo '{
    "html": "acronyctous cowskin Geometroidea epidosite mountainous arthritis cohosh ouphe pterygoidean shoepack nongentile interfuse counterorder geoparallelotropic niveous babushka cabook encephalometer ornamentally beartongue yore smilingly unimaginatively berkowitz",
    "links": [
        {
            "id": 608,
            "title": "Savanna",
            "uri": "http://unprintableness.com/unshowy/sylphon?a=fontinalis&b=praelabrum#houseowner"
        }
    ],
    "references": [
        {
            "anchor": "π",
            "position": 73,
            "link": 608
        }
    ]
}' | http POST localhost:1234/store/test
```

## Retrieving data
To get the JSON saved with the key `test` run the following shell command

```shell
http localhost:1234/store/test
```

# Project design
WikiDB is a wrapper around MongoDB to create a key/value storage service. As a wrapper it can be replicated, so that many version of it can talk to the same database, or cluster of databases. A cache layer was added using Redis, where only the HTML content of a blob is kept to accelerate reading speed, and the evition happens with a TTL of 5 minutes.

The handler struct manages database queries and api behavior, more information about it can be found on the function comments or generating godocs with 

```shell
make docs
```

The project uses protobuf to create the used entities, this choice was based on simplicity. Protobuf creates a good amount of boilerplate code. 
You can re-compile the entities to golang running the following command inside the folder `src/entities/`:

```shell
protoc -I. --go_out=. entities.proto
```

The golang plugin for Protobuf is necessary for that compilation.

The entities used are:

* Link: identifies the content from an URI with a title

* Reference: associates a link to a position in an HTML text

* Document: represents a JSON blob with links and references for its HTML content

## Folders
The project folders try to explain its structure:

* config: contains config files in a nginx-like syntax

* devops: contains docker files for infrastructure, eg. a mongoDB server

* vendor: has the library dependencies used in the project

* src: includes all code, vid godocs or code comments for more details

## Endpoints

| method | endpoint | 
| -------|----------|
| POST  | store/:key| 
| GET | store/:key| 
| GET | store/:key/:id| 
| GET | store/:key/:id/:section| 
| GET | cached/:key| 
