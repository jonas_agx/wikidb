/*
	package log is a simple wrapper on logrus logger
*/
package log

import (
	"os"

	"github.com/lytics/logrus"
)

type Logger struct {
	logrus.Logger
}

func New() *Logger {
	l := &Logger{}
	l.Out = os.Stdout
	l.Formatter = new(logrus.TextFormatter)
	l.Hooks = make(logrus.LevelHooks)
	l.Level = logrus.DebugLevel

	return l
}

// L global logger
var L *Logger

func init() {
	L = New()
}
