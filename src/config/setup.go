package config

import (
	"context"
	"flag"
	"fmt"
	"os"

	logger "bitbucket.org/jonas_agx/wikidb/src/log"
	"github.com/globalsign/mgo"
	"github.com/go-redis/redis"
)

var (
	Ctx        context.Context
	GlobalConf *Conf
)

// init is the first function running once the package config is loaded
func init() {
	GlobalConf = Setup()
	configCtx := context.WithValue(context.Background(), Conf{}, GlobalConf)
	Ctx = context.WithValue(configCtx, DB{}, InitDB(GlobalConf))
	Ctx = context.WithValue(Ctx, Cache{}, InitCache(GlobalConf))
	logger.L.Info("loaded global config")
}

// Setup reading a config file and have it loaded into a Conf structure
func Setup() *Conf {
	configFile := flag.String("config", "", "Path to service.conf")
	flag.Parse()

	if *configFile == "" {
		fmt.Fprintf(os.Stderr, "the -config file wasn't specified\n")
		flag.Usage()
		os.Exit(1)
	}

	c, err := LoadConf(*configFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "problem with file: %v", err)
		os.Exit(2)
	}

	return c
}

// InitDB connects to a database using the credentials provided by Conf, then returns a DB object
func InitDB(c *Conf) DB {
	db, err := mgo.Dial(GlobalConf.MongoHost)
	if err != nil {
		msg := fmt.Sprintf("failed to connect to mongo with: %v", err)
		panic(msg)
	}

	c.DB.Session = db

	return c.DB
}

func InitCache(c *Conf) Cache {
	cache := redis.NewClient(&redis.Options{
		Addr:     c.Cache.Host,
		Password: c.Cache.Password,
		DB:       c.Cache.Dabatase,
	})

	_, err := cache.Ping().Result()
	if err != nil {
		msg := fmt.Sprintf("failed to connect to redis with: %v", err)
		panic(msg)
	}

	c.Cache.Client = cache
	return c.Cache
}

// SuccessOrDie handles errors that can prevent the service from starting
func SuccessOrDie(e error) {
	if e != nil {
		logger.L.Errorf("failed with: %v", e)

		fmt.Fprintf(os.Stderr, "error: %v\n", e)
		os.Exit(1)
	}
}
