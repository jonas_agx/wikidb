/*
	package config is responsible for reading and loading a config file into a global context accessible across all packages of the application.
*/
package config

import (
	"github.com/globalsign/mgo"
	"github.com/go-redis/redis"
	"github.com/lytics/confl"
)

// Conf represents the data from a config file
type Conf struct {
	APIAddress string
	APIPort    string
	LogLevel   string
	MongoHost  string
	DB         DB
	Cache    Cache
}

// DB represents the data used to connect to a database.
// It also keeps a copy a database session
type DB struct {
	Host    string
	Name    string
	Session *mgo.Session
}

type Cache struct {
	Host     string
	Password string
	Dabatase int
	Client   *redis.Client
}

// LoadConf loads config data into a Conf struct. The filename provided must be a relative path to the config file.
// The config file should follow confl syntax
func LoadConf(filename string) (*Conf, error) {
	var c *Conf

	_, err := confl.DecodeFile(filename, &c)
	if err != nil {
		return nil, err
	}

	return c, nil
}
