/*
	package api maps endpoints and handlers
*/
package api

import (
	"bitbucket.org/jonas_agx/wikidb/src/api/handler"
	"bitbucket.org/jonas_agx/wikidb/src/config"
	logger "bitbucket.org/jonas_agx/wikidb/src/log"
	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
)

// SetupRoutes maps endpoints to handlers and starts the API server
func SetupRoutes(e *echo.Echo, h *handler.APIHandler) {
	e.POST("store/:key", h.StoreDocument)
	e.GET("store/:key", h.GetDocumentByKey)
	e.GET("store/:key/:id", h.GetDocumentByKeyAndID)
	e.GET("store/:key/:id/:section", h.GetDocumentBySection)
	e.GET("cached/:key", h.GetCackedHTML)

	// Start server
	e.Logger.Fatal(e.Start(config.GlobalConf.APIPort))
}

// SetupAPI creates the API layer with Echo and the Data layer with a db session.
func SetupAPI() {
	e := echo.New()
	e.Logger.SetLevel(log.DEBUG)
	e.Logger.SetOutput(logger.L.Out)

	db, ok := config.Ctx.Value(config.DB{}).(config.DB)
	if !ok {
		panic("failed to get db for API setup")
	}

	cache, ok := config.Ctx.Value(config.Cache{}).(config.Cache)
	if !ok {
		panic("failed to get cache for API setup")
	}

	// Initialize handler
	h := handler.NewHandler(db.Session, cache.Client)
	SetupRoutes(e, h)
}
