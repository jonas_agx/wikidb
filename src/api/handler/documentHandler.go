package handler

import (
	"crypto/sha256"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/jonas_agx/wikidb/src/entities"
	logger "bitbucket.org/jonas_agx/wikidb/src/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo"
)

var (
	// SortByField is the default field to sort document queries.
	// It starts with a - so the result is sorted in reverse order, the newer document first.
	SortByField = "-createdat"
)

// hash calculates the SHA-256 hash for a string s
func hash(s string) string {
	b := sha256.Sum256([]byte(s))
	return fmt.Sprintf("%x", b)
}

// StoreDocument stores a document using `key` as a key.
// The document ID is defined as a hash of the document's HTML
// so the same key can have different content versions, represented by the
// different IDs
//
// This function checks if the document already exists before inserting or not.
func (h *APIHandler) StoreDocument(c echo.Context) (err error) {
	d := new(entities.Document)
	if err = c.Bind(d); err != nil {
		return
	}

	key := c.Param("key")
	if key == "" {
		return NewErrResponse("key must be defined", http.StatusBadRequest, nil)
	}
	d.Key = key
	d.CreatedAt = time.Now().Unix()
	d.ID = hash(d.HTML)

	query := bson.M{"id": d.ID, "key": d.Key}
	err = h.Upsert(DocumentTable, d, query)
	if err = handleBadResponse(err, query); err != nil {
		return
	}

	cleanDocumentData(d)
	return c.JSON(http.StatusOK, d)
}

// GetDocumentByKey returns the content of the latest content for a key `key`
// or a not-found error if `key` is not present in the database
func (h *APIHandler) GetDocumentByKey(c echo.Context) (err error) {
	key := c.Param("key")

	query := bson.M{"key": key}
	d := new(entities.Document)

	err = h.Find(DocumentTable, query, d, SortByField)
	if err = handleBadResponse(err, query); err != nil {
		return
	}

	cleanDocumentMetafields(d)
	return c.JSON(http.StatusOK, d)
}

// GetDocumentByKeyAndID returns the content for key:ID
// or a not-found error if `key:ID` is not present in the database
func (h *APIHandler) GetDocumentByKeyAndID(c echo.Context) (err error) {
	key := c.Param("key")
	id := c.Param("id")

	if key == "" || id == "" {
		return NewErrResponse("key and id must be defined", http.StatusBadRequest, nil)
	}

	query := bson.M{"id": id, "key": key}
	d := new(entities.Document)

	err = h.Find(DocumentTable, query, d)
	if err = handleBadResponse(err, query); err != nil {
		return
	}

	cleanDocumentMetafields(d)
	return c.JSON(http.StatusOK, d)
}

// GetDocumentBySection returns a given specific section for a document `key:ID`
// or a not-found error if `key:ID:section` is not present in the database.
// A sorting field is not necessary since a `key:ID` is given, it also helps
// running queries faster,
func (h *APIHandler) GetDocumentBySection(c echo.Context) (err error) {
	key := c.Param("key")
	id := c.Param("id")
	section := c.Param("section")

	if key == "" || id == "" || section == "" {
		return NewErrResponse("key, id and section must be defined", http.StatusBadRequest, nil)
	}

	query := bson.M{"id": id, "key": key}
	selector := bson.M{section: 1}

	d := new(entities.Document)
	err = h.FindWithSelector(DocumentTable, query, selector, d)

	if err = handleBadResponse(err, query); err != nil {
		return
	}

	cleanDocumentMetafields(d)
	return c.JSON(http.StatusOK, d)
}

func handleBadResponse(err error, query interface{}) error {
	if err == nil {
		return nil
	}

	if err == mgo.ErrNotFound {
		return NewErrResponse("document not found", http.StatusNotFound, err)
	}

	msg := fmt.Sprintf("failed to get document with query: %v and error: %v", query, err)
	logger.L.Error(msg)

	return NewErrResponse(msg, http.StatusBadRequest, err)
}

func cleanDocumentMetafields(d *entities.Document) {
	if d != nil {
		d.Key = ""
		d.ID = ""
		d.CreatedAt = 0
	}
}

func cleanDocumentData(d *entities.Document) {
	if d != nil {
		d.HTML = ""
		d.Links = nil
		d.References = nil
	}
}
