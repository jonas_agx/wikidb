package handler

import (
	"net/http"
	"time"

	"bitbucket.org/jonas_agx/wikidb/src/entities"
	logger "bitbucket.org/jonas_agx/wikidb/src/log"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo"
)

const (
	// HTMLSection represents the section name with HTML content
	HTMLSection = "html"
	// TTL is the time to live for each cache item
	TTL = time.Minute * 5
)

// GetCackedHTML looks for content on cache, if it exists but is not cached copies it to cache and return.
// so next time the content is requested the response is faster.
//
// The cache eviction method is by timeout, with a TTL of 5 minutes.
// It may be a problem if the cache gets full within a shorter interval.
func (h *APIHandler) GetCackedHTML(c echo.Context) (err error) {
	key := c.Param("key")

	if key == "" {
		return NewErrResponse("key must be defined", http.StatusBadRequest, nil)
	}

	result := h.Cache.Get(key)
	d := new(entities.Document)

	if result.Val() == "" {
		logger.L.Debugf("cache miss for key:%s", key)

		query := bson.M{"key": key}
		selector := bson.M{HTMLSection: 1}

		err = h.FindWithSelector(DocumentTable, query, selector, d, SortByField)
		if err = handleBadResponse(err, query); err != nil {
			return
		}

		h.Cache.Set(key, d.HTML, TTL)
		return c.JSON(http.StatusOK, d)
	}

	d.HTML = result.Val()
	return c.JSON(http.StatusOK, d)
}
