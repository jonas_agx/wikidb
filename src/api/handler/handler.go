/*
	package handler manages how data is inserted and retrieved for the API
*/
package handler

import (
	"bitbucket.org/jonas_agx/wikidb/src/config"
	logger "bitbucket.org/jonas_agx/wikidb/src/log"
	"github.com/globalsign/mgo"
	"github.com/go-redis/redis"
	"github.com/labstack/echo"
)

// APIHandler holds all API operations and their communication with a database session.
// A session represents a communication session with the database.

// All Session methods are concurrency-safe and may be called from multiple goroutines.
type APIHandler struct {
	DB    *mgo.Session
	Cache *redis.Client
}

var (
	DocumentTable = "documents"
)

// NewHandler creates a new handler from a database session
func NewHandler(db *mgo.Session, cache *redis.Client) *APIHandler {
	return &APIHandler{DB: db, Cache: cache}
}

// Find searches in a collection using a query. If an entry is found result will be populated, if not, an not-found error is returned.
func (h *APIHandler) Find(collection string, query, result interface{}, sortBy ...string) (err error) {
	db := h.DB.Clone()
	defer db.Close()

	err = db.DB(config.GlobalConf.DB.Name).C(collection).Find(query).Sort(sortBy...).One(result)
	if err != nil {
		logger.L.Errorf("failed to get document with query: %v and error: %v", query, err)
	} else {
		logger.L.Debugf("successfully found document with: %v", query)
	}

	return
}

// FindWithSelector retrieves only parts of an entity so the query execution is faster.
// The selected fields are defined by selectors
// sortBy  asks the database to order returned documents according to the
// provided field names. A field name may be prefixed by - (minus) for
// it to be sorted in reverse order.
func (h *APIHandler) FindWithSelector(collection string, query, selector, result interface{}, sortBy ...string) (err error) {
	d := h.DB.Clone()
	defer d.Close()

	err = d.DB(config.GlobalConf.DB.Name).C(collection).Find(query).Sort(sortBy...).Select(selector).One(result)
	if err != nil {
		logger.L.Errorf("failed to get document with query: %v and error: %v", query, err)
	} else {
		logger.L.Debugf("successfully found document with: %v", query)
	}

	return
}

// Upsert finds a single document matching the provided selector document
// and modifies it according to the update document.  If no document matching
// the selector is found, the update document is applied to the selector
// document and the result is inserted in the collection.
func (h *APIHandler) Upsert(collection string, entity, query interface{}) (err error) {
	db := h.DB.Clone()
	defer db.Close()
	_, err = db.DB(config.GlobalConf.DB.Name).C(collection).Upsert(query, entity)

	if err != nil {
		logger.L.Errorf("failed to store document with query: %v and error: %v", query, err)
	} else {
		logger.L.Debugf("successfully stored document with: %v", query)
	}

	return
}

func NewErrResponse(msg string, code int, err error) *echo.HTTPError {
	return &echo.HTTPError{Code: code, Message: msg, Internal: err}
}
