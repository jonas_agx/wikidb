all: build run

build:
	go build -o bin/api src/cmd/api/main.go

run:
	./bin/api -config=config/service.conf

docker-compose-up:
	docker-compose --file devops/docker/docker-compose.yml up -d

docker-compose-down:
	docker-compose --file ./devops/docker/docker-compose.yml down

clean:
	rm -rf bin

docs:
	echo "check docs at http://localhost:8080/pkg/bitbucket.org/jonas_agx/wikidb/"
	godoc -http=":8080"